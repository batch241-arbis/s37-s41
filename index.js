// Set up the dependencies
const express = require(`express`);
const mongoose = require(`mongoose`);
// Allows our backend application to be available to our frontend application
const cors = require(`cors`);
const userRoute = require("./Routes/userRoute");
const courseRoute = require("./Routes/courseRoute");
const userContoller = require("./Controllers/userController")
// Server Setup
const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.eua2rv1.mongodb.net/B241-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/users", userRoute);
app.use("/courses", courseRoute)


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"));

app.listen(port, () => console.log(`API is now online on port ${port}`));