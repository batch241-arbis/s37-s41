const Course = require("../Models/Course");

module.exports.addCourse = (user, reqBody) => {
	if (user.isAdmin == true){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return false
	}
}


// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	/*
	Syntax:
		findByIdAndUpdate(documentId, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// Archiving a course
module.exports.archiveCourse = (isAdmin, reqParams) => {
	if (isAdmin == true) {
	return Course.findByIdAndUpdate(reqParams.courseId, {isActive: false}).then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
	} else {
		return false
	}
}