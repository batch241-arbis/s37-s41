const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController");
const auth = require("../auth");


// Route for creating a course
router.post ("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.addCourse({isAdmin: userData.isAdmin}, req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all the courses
router.get("/all", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Route for getting active courses
router.get("/activecourses", (req,res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
});

// Retrieve specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData.isAdmin);
	courseController.archiveCourse({isAdmin: userData.isAdmin}, req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router;